<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="style.css"/>
        <title>Third Task</title>
    </head>
    <body>
        <form id="myForm" method="POST" action="">
			<label>
				 Имя: <br>
				 <input type="text" name="name" />
	  
			</label>
			<br>
			<label>
			  Email: <br>
			  <input type="email" name="email" />
	  
			</label>
	  
			<br>
		   <label>
		  Дата рождения: <br>
		  <select name="birth" >
			 <option value="2000">2000</option>
			 <option value="2001">2001</option>
			 <option value="2002">2002</option>
		  </select>
	  
		  </label>
		 <br>
		 Пол:<br>
		 <label>
		  
		  <input type="radio" checked="checked" value="0" name="gender">
		  Женский
		 </label>
		 <br>
		<label>
		  
		  <input type="radio"  value="1" name="gender">
		  Мужской
		</label>
		<br>
		Количество конечностей:
		<br>
		<label>
		 
		 <input type="radio" value="0" name="counter">
		 0
		</label>
	   <br>
		<label>
		 
		 <input type="radio" value="gt0" name="counter">
		 > 0
		</label>
		<br>
		<label>
		  Сверхспособности:
		  <br>
		  <select name="options[]" multiple>
			 <option value="nodeath">бессмертие</option>
			 <option value="through">прохождение сквозь стены</option>
			 <option value="levitation">левитация</option>
		  </select>
		</label>
		<br>
		<label>
		  Биография:
		  <br>
		  <textarea name="info">
	  
		  </textarea>
		</label>
	   <br>
		<label>
		  
		  <input type="checkbox" checked="checked" name="checked">
		  С контрактом ознакомлен
		</label>
	  
	   <br>
		  <input type="submit" value="Отправить">
		</form>
    </body>
</html>