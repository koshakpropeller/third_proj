<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/**
 * Интернет-программирование. Задача 8.
 * Реализовать скрипт на веб-сервере на PHP или другом языке,
 * сохраняющий в XML-файл заполненную форму задания 7. При
 * отправке формы на сервере создается новый файл с уникальным именем.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
  if (!empty($_GET['save'])) {
    // Если есть параметр save, то выводим сообщение пользователю.
    print('Спасибо, результаты сохранены.');
  }
  // Включаем содержимое файла form.php.
  include('form.php');
  // Завершаем работу скрипта.
  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки.
$errors = FALSE;
if (empty($_POST['name']) || strlen($_POST['name']) > 128) {
  print('Заполните имя.<br/>');
  $errors = TRUE;
}
if(preg_match('/[^а-яА-Яa-zA-Z\s]+/msi',$_POST['name'])) {
    print("Имя должно состоять только из русский или английских букв и пробелов");
    $errors = TRUE;
}
if (empty($_POST['email'])) {
    print('Заполните поле email.<br/>');
    $errors = TRUE;
}
if (empty($_POST['birth'])) {
    print("Заполните поле дата рождения");
    $errors = TRUE;
}
if (!isset($_POST['gender'])) {
    print("Выберете пол");
    $errors = TRUE;
}
if (!isset($_POST['counter'])) {
    print("Выберете количество конечностей");
    $errors = TRUE;
}
if (!isset($_POST["options"])) {
    print("А где ваши способности?");
    $errors = TRUE;
}
if (empty($_POST["info"])) {
    print("Кто вы?Поподробнее");
    $errors = TRUE;
}
if(!isset($_POST["checked"]) || $_POST["checked"] == 'Yes') {
    print("Согласитесь с нами");
    $errors = TRUE;
}
// *************
// Тут необходимо проверить правильность заполнения всех остальных полей.
// *************

if ($errors) {
  // При наличии ошибок завершаем работу скрипта.
  exit();
}

// Сохранение в базу данных.

$user = 'u20386';
$pass = '2551027';
$db = new PDO('mysql:host=localhost;dbname=u20386', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

// Подготовленный запрос. Не именованные метки.



try {
  $stmt = $db->prepare("INSERT INTO app (name, email, year, gender, lungs, nodeath, through, levitation, biography) VALUES (:name, :email, :year, :gender, :lungs, :nodeath, :through, :levitation, :biography)");
  $name = $_POST['name'];
  $stmt->bindParam(':name', $name, PDO::PARAM_STR);
  
  $email = $_POST['email'];
  $stmt->bindParam(':email', $email,PDO::PARAM_STR);
 
  $year = $_POST['birth'];
  $stmt->bindParam(':year', $year, PDO::PARAM_INT); 
  
  $gender = $_POST['gender'];
  $stmt->bindParam(':gender', $gender, PDO::PARAM_INT); 
  
  $counter = $_POST['counter'];
  $stmt->bindParam(':lungs', $counter, PDO::PARAM_INT);
  
  $nodeath = in_array("nodeath",$_POST["options"]);

  $stmt->bindParam(':nodeath', $nodeath, PDO::PARAM_BOOL);
  
  $through = in_array("through",$_POST["options"]);

  $stmt->bindParam(':through', $through,PDO::PARAM_BOOL);
  
  $levitation = in_array("levitation", $_POST["options"]);
  
  $stmt->bindParam(':levitation', $levitation,PDO::PARAM_BOOL);
  
  $biography = $_POST["info"];
  print($biography);
  $stmt->bindParam(':biography', $biography, PDO::PARAM_STR);
  
 
 
  $stmt -> execute();
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

//  stmt - это "дескриптор состояния".
 
//  Именованные метки.
//$stmt = $db->prepare("INSERT INTO test (label,color) VALUES (:label,:color)");
//$stmt -> execute(array('label'=>'perfect', 'color'=>'green'));
 
//Еще вариант
/*$stmt = $db->prepare("INSERT INTO users (firstname, lastname, email) VALUES (:firstname, :lastname, :email)");
$stmt->bindParam(':firstname', $firstname);
$stmt->bindParam(':lastname', $lastname);
$stmt->bindParam(':email', $email);
$firstname = "John";
$lastname = "Smith";
$email = "john@test.com";
$stmt->execute();
*/

// Делаем перенаправление.
// Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.
header('Location: ?save=1');
